<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Fjalla+One&display=swap" rel="stylesheet">

    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">

		<!-- header -->

	<?php

	$posttitle = "Header-page"; // check title page
	$postid = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_title = '" . $posttitle . "'" );
	$p = get_page($postid);
	$t = $p->post_title;
	echo apply_filters('the_content', $p->post_content);

	?>

	<div class="modal fade" id="search_modal" role="dialog">
		<form style="padding: 20px" method="get" id="searchform" action="<?php bloginfo('url'); ?>/">
			<div class="close-search" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" fill="#fff" viewBox="0 0 47.971 47.971" style="enable-background:new 0 0 47.971 47.971;/* fill: #fff; */" xml:space="preserve">
							<g>
								<path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88   c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242   C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879   s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/>
							</g>
						</svg>
					</span>
			</div>
			<input style="background: transparent; border: 0px; padding-left: 35px; padding-top: 0px" type="text" class="input-search" placeholder="Search Keyword" value="<?php the_search_query(); ?>" name="s" id="s" />
			<button type="submit" id="searchsubmit" value="">
				<i class="fa fa-search"></i>
			</button>
		</form>
	</div>






