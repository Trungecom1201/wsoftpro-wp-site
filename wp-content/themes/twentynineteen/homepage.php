<div class="container homepage-content">
    <div class="effective-content">
        <div class="middle-line"><img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/vertical_line.png"></div>
        <h2 class="effective-title">
            <div>We are the one of the most effective</div>
            <div>Web Design Companies</div>
        </h2>
        <p class="effective-description">Getting online is easy. Succeeding online is a different story. You’ll need more than just a beautiful website to stand out these days.<b>Online marketing solutions</b> . Conversion-based web design coupled with a lead generating marketing plan, your online success is inevitable.</p>
        <div class="middle-line"><img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/vertical_line.png"></div>
        <a class="button-theme get-started" href="#">Get Started</a>
        <div class="middle-line"><img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/vertical_line.png"></div>
    </div>

    <div class="service">
        <div class="service-option">
            <div class="service-image"><a href=""><img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/service_icon_1-1.png" alt=""></a></div>
            <h4 class="service-title"><a href="">Marketing</a></h4>
            <div class="service-info">We use strategic marketing tactics that have been proven to work.</div>
        </div>
        <div class="service-option">
            <div class="service-image"><a href=""><img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/service_icon_2-1.png" alt=""></a></div>
            <h4 class="service-title"><a href="">Development</a></h4>
            <div class="service-info">Custom programming for most complex functions you can think.</div>
        </div>
        <div class="service-option">
            <div class="service-image"><a href=""><img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/service_icon_3-1.png" alt=""></a></div>
            <h4 class="service-title"><a href="">Web Design</a></h4>
            <div class="service-info">Powerful web design that will out-perform your strongest competitors.</div>
        </div>
        <div class="service-option">
            <div class="service-image"><a href=""><img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/service_icon_4-1.png" alt=""></a></div>
            <h4 class="service-title"><a href="">SEO Optimization</a></h4>
            <div class="service-info">Optimizing our web designs to rank on the first page of google is our specialty.</div>
        </div>
        <div class="service-option">
            <div class="service-image"><a href=""><img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/service_icon_5-1.png" alt=""></a></div>
            <h4 class="service-title"><a href="">Ecommerce</a></h4>
            <div class="service-info">We build your online store using a flexible, modular platform that allows you to expand…</div>
        </div>
        <div class="service-option">
            <div class="service-image"><a href=""><img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/service_icon_6-1.png" alt=""></a></div>
            <h4 class="service-title"><a href="">Branding</a></h4>
            <div class="service-info">A solid brand strategy, logo and guidelines help you to get You recognized.</div>
        </div>
    </div>
    <div class="project">
        <div class="content-project">
            <div class="step-project">
                <div class="step-left">
                    <h2 class="step-title color1">Strategy</h2>
                </div>
                <div class="step-center">
                    <img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/img_8-600x600.jpg" alt="">
                </div>
                <div class="step-right">We define your competition and target audience. Discover what is working in your online industry, then design your website accordingly.</div>
            </div>
            <div class="step-project">
                <div class="step-left">
                    <h2 class="step-title">Design</h2>
                </div>
                <div class="step-center">
                    <img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/img_9-600x600.jpg" alt="">
                </div>
                <div class="step-right">Color scheme, layout, sitemap, and style. We will bring your brand to life with a one of a kind masterpiece, built just for you.</div>
            </div>
            <div class="step-project">
                <div class="step-left">
                    <h2 class="step-title">Develop</h2>
                </div>
                <div class="step-center">
                    <img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/img_2-600x600.jpg" alt="">
                </div>
                <div class="step-right">We turn your ideas into a reality. &amp;our website is placed on a "development server" where you get to watch the whole process, live.</div>
            </div>
            <div class="step-project">
                <div class="step-left">
                    <h2 class="step-title">Support</h2>
                </div>
                <div class="step-center  last">
                    <img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/img_3-600x600.jpg" alt="">
                </div>
                <div class="step-right">This is where you go live, to the world. Design, marketing, and maintenance; we'll be at your side for the life of your site.</div>
            </div>
            <div class="middle-line"><img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/vertical_line2.png"></div>
        </div>
    </div>
    <div class="after-project">
        <div style="margin-top: -35px"></div>
        <a class="button-theme get-started" href="#">Get Started</a>
    </div>
    <div class="middle-line"><img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/vertical_line.png"></div>
    <div class="service-list">
        <div class="list">
            <a href="#" data-filter="*">All</a>
            <a href="#" data-filter=".web-design-shortcode" class="">Web Design</a>
            <a href="#" data-filter=".averticement-shortcode" class="">Averticement</a>
            <a href="#" data-filter=".logo-design-shortcode" class="">Logo Design</a>
            <a href="#" data-filter=".branding-shortcode">Branding</a>
            <a href="#" data-filter=".photo-shortcode">Photo</a>
            <a href="#" data-filter=".design-shortcode">Design</a>
        </div>
        <div class="slider-service">[carousel_slide id='70']</div>
    </div>
    <div class="slider-team">
        <div id="carousel" class="flexslider name-member">
            <ul class="slides">
                <li>
                    Letha L. Young                        <span class="position">Marketer</span>
                </li>
                <li>
                    Harold D. Cote                        <span class="position">Developer</span>
                </li>
                <li>
                    Oren R. Odom                        <span class="position">Marketer</span>
                </li>
                <li>
                    Gregory F. Parrino                        <span class="position">CEO</span>
                </li>
                <!-- items mirrored twice, total of 12 -->
            </ul>
        </div>
        <div id="slider" class="flexslider image-member">
            <ul class="slides">
                <li>
                    <img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/team_slide_01.jpg">
                </li>
                <li>
                    <img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/team_slide_02.jpg">
                </li>
                <li>
                    <img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/team_slide_03.jpg">
                </li>
                <li>
                    <img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/team_slide_04.jpg">
                </li>
                <!-- items mirrored twice, total of 12 -->
            </ul>
        </div>

    </div>
    
    <div class="middle-line"><img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/vertical_line.png"></div>
    <div class="get-start">
        <div class="title">
            <div>Lets Get Started</div>
            <div>Your Project</div>
        </div>
        <div class="description">We’ll help to achieve your goals and to grow business</div>
    </div>
    <div class="middle-line"><img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/vertical_line.png"></div>
    <a class="button-theme lets-talk" href="#">LET'S TALK</a>
    <div class="middle-line">
        <div class="skew-left"></div>
        <div class="skew-right"></div>
        <img src="http://wsoftpro.com/wsoftpro-wp/wp-content/uploads/2019/05/vertical_line.png">
    </div>
    <div class="contact-us">
        <div class="container">
            <div class="contact-item">
                <div class="ct-icon">
                    <div class="ct-icon-round">
                        <div class="ct-icon-circle">
                            <img src="https://image.flaticon.com/icons/svg/977/977411.svg" alt="">
                            
                        </div>
                    </div>
                </div>
                <div class="title">Call Us</div>
                <div class="description">
                    <div><span class="sub-title">New Accounts:</span> <span class="sub-info">1-800-123-4567</span></div>
                    <div><span class="sub-title">Support</span> <span class="sub-info">1-800-123-4569</span></div>
                </div>
            </div>
            <div class="contact-item">
                <div class="ct-icon">
                    <div class="ct-icon-round">
                        <div class="ct-icon-circle">
                            <img src="https://image.flaticon.com/icons/svg/660/660756.svg" alt="">
                        </div>
                    </div>
                </div>
                <div class="title">Write Us</div>
                <div class="description">
                    <div>
                        <a href="mailto:" class="sub-info">example@example.com</a>
                    </div>
                    <div>
                        <a href="mailto:" class="sub-info">example@example.com</a>
                    </div>
                </div>
            </div>
            <div class="contact-item">
                <div class="ct-icon">
                    <div class="ct-icon-round">
                        <div class="ct-icon-circle">
                            <img src="https://image.flaticon.com/icons/svg/771/771617.svg" alt="">
                        </div>
                    </div>
                </div>
                <div class="title">Visit Us</div>
                <div class="description">
                    <div><span class="sub-info">2231 Sycamore Lake Road<br>
        Green Bay, WI 54304</span></div>
                </div>
            </div>
        </div>
    </div>
</div>