<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( ! twentynineteen_can_show_post_thumbnail() ) : ?>
<!--	<header class="entry-header">-->
<!--        <i class="fas fa-circle"></i>-->
<!--        --><?php //get_template_part( 'template-parts/header/entry', 'header' ); ?>
<!--        <ol id="breadcrumb" class="container_12 breadcrumb">-->
<!--            --><?php //bcdonline_breadcrumbs(); ?>
<!--        </ol>-->
<!--	</header>-->
	<?php endif; ?>

	<div class="entry-content">
		<?php
		the_content();

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'twentynineteen' ),
				'after'  => '</div>',
			)
		);
		?>
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
