<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wsoftpro_wp' );

/** MySQL database username */
define( 'DB_USER', 'wsoftpro_wp' );

/** MySQL database password */
define( 'DB_PASSWORD', 'g7wSpyLv' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '! w$19!H-oLQ=glSMP9)1d{oOk4J``7_#y.$->/6Z`kZZP+m4DLl}*M{[{xuPS5Y' );
define( 'SECURE_AUTH_KEY',  ',G1:%^z/34~|.y1R-SW||k6[BqQG9+}&Q+y-1F8w*DY^fS0U7$LE+5l[`ZT+y.(5' );
define( 'LOGGED_IN_KEY',    'sA!w;QHfcsN)mi#D/jPJap(ir:;@K[BM)5_$Rq=`Z<OOJSBt<nbQmXXU7.Dz<,}D' );
define( 'NONCE_KEY',        '[HY2lwxp!!&;7W}y^l(?KBYD5kruc1}vSKfYVa~h;G&z3r1DNt4E&O.j~cORmL5i' );
define( 'AUTH_SALT',        'lgjrAR8)uB?9d+aO>VhTx,J+v~85ZZ]>(g)YI6Z$Y&xA;0l^TrKwV<sUFrbV8&I=' );
define( 'SECURE_AUTH_SALT', '|D=}}qCUe9n7ZpU 7 mVo?hX5k4q:x7cB^}}ZGIB]R2,~B?^=~KHD0fcOzO6L2Jy' );
define( 'LOGGED_IN_SALT',   '/?`yFsPWb$o53niutO4]l/YuNJtq1+$ffg$4lDlQ4jN]h}@M0a{#m2&O2p7sXZtc' );
define( 'NONCE_SALT',       '!qHp8X4fpF#[&v_3paV%X#SRrn7b*osa7LpRu2Oe+[1!J7->u^(%-%6Iaz3N!@;a' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
